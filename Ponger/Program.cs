﻿using System;
using System.Text;
using PingPongLibrary;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Ponger
{
    class Program
    {
        private static readonly RabbitMQWrapper pingpong = new RabbitMQWrapper();
        
        static void Main(string[] args)
        {
            PingPongSettings.ListenQueue = "pong_queue";
            PingPongSettings.WriteToQueue = "ping_queue";
            PingPongSettings.Message = "pong";
            PingPongSettings.ReadRoutingKey = "pong_queue";
            PingPongSettings.WriteRoutingKey = "ping_queue";

            pingpong.Received += (o, e) =>
            {
                pingpong.PrintMessageToConsole(Encoding.UTF8.GetString(e.Body.ToArray()));
                pingpong.Channel.BasicAck(e.DeliveryTag, false);
                pingpong.SendMessageToQueue();
            };
            
            while (true)
            {
                pingpong.ListenQueue();
            }
        }
    }
}