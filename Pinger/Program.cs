﻿using System;
using System.Text;
using PingPongLibrary;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Pinger
{
    class Program
    {
        private static readonly RabbitMQWrapper pingpong = new RabbitMQWrapper();

        static void Main(string[] args)
        {
            PingPongSettings.ListenQueue = "ping_queue";
            PingPongSettings.WriteToQueue = "pong_queue";
            PingPongSettings.Message = "ping";
            PingPongSettings.ReadRoutingKey = "ping_queue";
            PingPongSettings.WriteRoutingKey = "pong_queue";

            pingpong.Received += (o, e) =>
            {
                pingpong.PrintMessageToConsole(Encoding.UTF8.GetString(e.Body.ToArray()));
                pingpong.Channel.BasicAck(e.DeliveryTag, false);
                pingpong.SendMessageToQueue();
            };
            
            pingpong.SendMessageToQueue();
            while (true)
            {
                pingpong.ListenQueue();
            }
        }
    }
}