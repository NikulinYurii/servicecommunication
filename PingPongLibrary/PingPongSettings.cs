﻿using RabbitMQ.Client;

namespace PingPongLibrary
{
    public static class PingPongSettings
    {
        public static string RabbitMqUrl { get; set; } = "localhost";
        public static string ReadRoutingKey { get; set; }
        public static string WriteRoutingKey { get; set; }
        public static int Time { get; set; } = 2500;
        public static string Exchange { get; set; } = "Exchange";
        public static string ListenQueue { get; set; }
        public static string WriteToQueue { get; set; }
        public static string Message { get; set; }

    }
}