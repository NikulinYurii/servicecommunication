﻿using System;
using System.Text;
using System.Threading;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace PingPongLibrary
{
    public class RabbitMQWrapper : IDisposable
    {
        public event EventHandler<BasicDeliverEventArgs> Received;
        public IModel Channel { get; private set; }

        private EventingBasicConsumer _consumer;
        private IConnection _connection;

        public void ListenQueue()
        {
            var factory = new ConnectionFactory() {HostName = PingPongSettings.RabbitMqUrl};
            _connection = factory.CreateConnection();
            Channel = _connection.CreateModel();
            Channel.ExchangeDeclare(PingPongSettings.Exchange, ExchangeType.Direct);
            Channel.QueueDeclare(
                queue: PingPongSettings.ListenQueue,
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);

            Channel.QueueBind(PingPongSettings.ListenQueue, PingPongSettings.Exchange,
                PingPongSettings.ReadRoutingKey);

            _consumer = new EventingBasicConsumer(Channel);
            _consumer.Received += this.Received;

            Channel.BasicConsume(
                queue: PingPongSettings.ListenQueue,
                autoAck: true,
                consumer: _consumer);
        }

        public void SendMessageToQueue()
        {
            Console.WriteLine(DateTime.Now +
                              $" WriteToQueue {PingPongSettings.WriteToQueue}, " +
                              $"Message {PingPongSettings.Message}, " +
                              $"RoutingKey {PingPongSettings.WriteRoutingKey}.");
            
            Thread.Sleep(PingPongSettings.Time);
            
            var factory = new ConnectionFactory() {HostName = PingPongSettings.RabbitMqUrl};
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(PingPongSettings.Exchange, ExchangeType.Direct);
                
                var body = Encoding.UTF8.GetBytes(PingPongSettings.Message);

                channel.BasicPublish(
                    exchange: PingPongSettings.Exchange,
                    routingKey: PingPongSettings.WriteRoutingKey,
                    basicProperties: null,
                    body: body);
            }
        }
        
        public void PrintMessageToConsole(string message)
        {
            Console.WriteLine(DateTime.Now +" "+ message);
        }

        public void Dispose()
        {
            _connection?.Dispose();
            Channel?.Dispose();
        }
    }
}